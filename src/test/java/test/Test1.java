package test;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQBytesMessage;
import org.apache.activemq.command.ActiveMQQueue;
import org.fusesource.mqtt.client.BlockingConnection;
import org.fusesource.mqtt.client.Callback;
import org.fusesource.mqtt.client.Future;
import org.fusesource.mqtt.client.FutureConnection;
import org.fusesource.mqtt.client.MQTT;
import org.fusesource.mqtt.client.Message;
import org.fusesource.mqtt.client.QoS;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class Test1 {

	private String serectKey = "waretrysfaegsrdtxfysctluyrthergwef";

	@Test
	public void testAMQPToMQTT() throws Exception {
		JwtBuilder builder = Jwts.builder();
		Calendar instance = Calendar.getInstance();
		builder.setExpiration(new Date(instance.getTime().getTime() + 3600000));

		builder.setHeaderParam("allowTopic", "test0");
		builder.setIssuedAt(instance.getTime());

		builder.signWith(SignatureAlgorithm.HS256, serectKey);

		String jwt = builder.compact();

		ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(jwt, null,
				"tcp://127.0.0.1:61616");

		Connection connection = activeMQConnectionFactory.createConnection();

		connection.start();

		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

		Topic topic = session.createTopic("test0");
		MessageProducer producer = session.createProducer(topic);
		// NON_PERSISTENT 非持久化 PERSISTENT 持久化,发送消息时用使用持久模式
		producer.setDeliveryMode(DeliveryMode.PERSISTENT);
		BytesMessage byteMessage = session.createBytesMessage();

		byte[] playload = new byte[] { 1, 2, 3 };

		byteMessage.writeBytes(playload);
		// 发布主题消息

		MQTT mqtt = new MQTT();

		mqtt.setUserName(jwt);
		mqtt.setHost("tcp://127.0.0.1:1883");

		FutureConnection futureConnection = mqtt.futureConnection();
		futureConnection.connect();
		futureConnection.subscribe(new org.fusesource.mqtt.client.Topic[] {
				new org.fusesource.mqtt.client.Topic("test", QoS.AT_LEAST_ONCE) });

		Future<Message> receive = futureConnection.receive();

		producer.send(byteMessage);

		receive.then(new Callback<Message>() {

			@Override
			public void onSuccess(Message value) {
				Assertions.assertTrue(Arrays.equals(playload, value.getPayload()));
			}

			@Override
			public void onFailure(Throwable value) {
				Assertions.fail();
			}
		});
	}

	@Test
	public void testMQTTToQueue() throws Exception {
		JwtBuilder builder = Jwts.builder();
		
		Date time = new Date();
		builder.setExpiration(new Date(time.getTime()+3600000));

		builder.setHeaderParam("allowTopic", "test.test");
		builder.setHeaderParam("allowQueue", "test.test");
		builder.setIssuedAt(time);

		builder.signWith(SignatureAlgorithm.HS256, serectKey);

		String jwt = builder.compact();

		MQTT mqtt = new MQTT();

		mqtt.setUserName(jwt);
		mqtt.setHost("tcp://127.0.0.1:1883");

		BlockingConnection blockingConnection = mqtt.blockingConnection();
		blockingConnection.connect();

		byte[] playload = new byte[] { 1, 2, 3 };

		blockingConnection.publish("/test/test", playload, QoS.AT_LEAST_ONCE, false);

		ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(jwt, null,
				"tcp://127.0.0.1:61616");

		Connection connection = activeMQConnectionFactory.createConnection();

		connection.start();

		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

		MessageConsumer createConsumer = session.createConsumer(new ActiveMQQueue("test.test"));

		ActiveMQBytesMessage receive = (ActiveMQBytesMessage) createConsumer.receive(1000);
		if (receive != null) {
			Assertions.assertTrue(Arrays.equals(playload, receive.getContent().getData()));
		} else {
			Assertions.fail();
		}
	}

}
