package test;

import java.util.Calendar;
import java.util.Date;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQDestination;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.fusesource.mqtt.client.BlockingConnection;
import org.fusesource.mqtt.client.MQTT;
import org.fusesource.mqtt.client.Message;
import org.fusesource.mqtt.client.QoS;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class Main {
	public static void main(String[] args) throws Exception {
		MQTT mqtt = new MQTT();

		
		Calendar instance = Calendar.getInstance();
		
		JwtBuilder builder = Jwts.builder();
		builder.setExpiration(new Date(instance.getTime().getTime() + 3600000));

		builder.setHeaderParam("allowTopic", "test");
		builder.setIssuedAt(instance.getTime());

		builder.signWith(SignatureAlgorithm.HS256, "waretrysfaegsrdtxfysctluyrthergwef");

		String jwt = builder.compact();

		mqtt.setHost("tcp://127.0.0.1:1883");
		mqtt.setUserName(jwt);

		BlockingConnection blockingConnection = mqtt.blockingConnection();
		blockingConnection.connect();
		
		blockingConnection.subscribe(new org.fusesource.mqtt.client.Topic[] {
				new org.fusesource.mqtt.client.Topic("TEST", QoS.AT_LEAST_ONCE) });
		 

		ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(jwt, null,
				"tcp://127.0.0.1:61616");

		{
			Connection connection = activeMQConnectionFactory.createConnection();

			connection.start();

			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

//			MessageConsumer createConsumer = session.createConsumer(new ActiveMQTopic("TEST"));

			Topic topic = session.createTopic("test");
			MessageProducer producer = session.createProducer(topic);
			// NON_PERSISTENT 非持久化 PERSISTENT 持久化,发送消息时用使用持久模式
			producer.setDeliveryMode(DeliveryMode.PERSISTENT);
			BytesMessage byteMessage = session.createBytesMessage();
			


			byteMessage.writeBytes(new byte[] { 1, 2, 3 });
			// 发布主题消息
			producer.send(byteMessage);

			return;
		}

//		 blockingConnection.publish("a.TEST", new byte[] { 0 }, QoS.AT_LEAST_ONCE,
//		 true);

		 

	}
}
