package test;

import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.Date;

import org.fusesource.mqtt.client.BlockingConnection;
import org.fusesource.mqtt.client.MQTT;
import org.fusesource.mqtt.client.Message;
import org.fusesource.mqtt.client.QoS;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class Main0 {
	public static void main(String[] args) throws Exception {
		MQTT mqtt = new MQTT();

		JwtBuilder builder = Jwts.builder();
		Calendar instance = Calendar.getInstance();
		builder.setExpiration(new Date(instance.getTime().getTime() + 3600000));

		builder.setHeaderParam("allowTopic", "test");
		builder.setIssuedAt(instance.getTime());

		builder.signWith(SignatureAlgorithm.HS256, "waretrysfaegsrdtxfysctluyrthergwef");

		String jwt = builder.compact();

		mqtt.setHost("tcp://127.0.0.1:1883");
		mqtt.setUserName(jwt);

		BlockingConnection blockingConnection = mqtt.blockingConnection();
		blockingConnection.connect();

		blockingConnection.subscribe(new org.fusesource.mqtt.client.Topic[] {
				new org.fusesource.mqtt.client.Topic("test", QoS.AT_LEAST_ONCE) });

		Message receive = blockingConnection.receive();
		
		byte[] payload = receive.getPayload();
		
		return;
	}
}
