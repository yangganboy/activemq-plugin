package test;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Test0 {
	public static void main(String[] args) throws NoSuchAlgorithmException, ClassNotFoundException, SQLException {
		MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");

		Class.forName("com.mysql.jdbc.Driver");

		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/hwwp?serverTimezone=Hongkong", "root", "havepassword");

		PreparedStatement prepareStatement = connection
				.prepareStatement("insert into endpoint_verify_info values(?,?,?,?,?,?,?,?)");
		prepareStatement.setLong(1, 1);
		prepareStatement.setString(2, "123456");
		prepareStatement.setBytes(3,
				messageDigest.digest(("123456" + "afsghdfjydsefdwafesgrdhtjy").getBytes(StandardCharsets.UTF_8)));
		prepareStatement.setByte(4, (byte) 0);
		prepareStatement.setInt(5, 1);
		prepareStatement.setTimestamp(6, null);
		prepareStatement.setByte(7, (byte) 0);
		prepareStatement.setBoolean(8, false);
		prepareStatement.executeUpdate();

		prepareStatement.close();
		connection.close();

	}
}
