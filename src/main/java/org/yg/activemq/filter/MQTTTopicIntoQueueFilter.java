package org.yg.activemq.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerFilter;
import org.apache.activemq.broker.ProducerBrokerExchange;
import org.apache.activemq.command.ActiveMQDestination;
import org.apache.activemq.command.Message;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.yg.activemq.util.MatchUtil;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MQTTTopicIntoQueueFilter extends BrokerFilter {

	private Logger log = LogManager.getLogger(this.getClass());

	private Map<String, List<String>> transpondRule;

	public void setTranspondRule(String transpondRuleStr) throws JsonProcessingException, IOException {
		transpondRule = new HashMap<>();

		JsonNode readTree = new ObjectMapper().readTree(transpondRuleStr);

		if (readTree.isArray()) {
			
			Iterator<JsonNode> elements = readTree.elements();

			while (elements.hasNext()) {
				List<String> queues = new ArrayList<>();
				JsonNode node = elements.next();
				String match = node.get("match").asText();
				JsonNode target = node.get("target");
				if (target.isArray()) {
					log.info("isArray");
					Iterator<JsonNode> nodeQueses = target.elements();
					while (nodeQueses.hasNext()) {
						queues.add(nodeQueses.next().asText());
					}
				} else {
					queues.add(target.asText());
				}

				transpondRule.put(match, queues);
			}
		} else {
			throw new IllegalArgumentException("mqtt.transpondRule is not a array");
		}

		log.info("transpondRule " + transpondRule);
	}

	public MQTTTopicIntoQueueFilter(Broker next) throws JsonProcessingException, IOException {
		super(next);
	}

	@Override
	public void send(ProducerBrokerExchange producerExchange, Message messageSend) throws Exception {
		Iterator<Entry<String, List<String>>> iterator = transpondRule.entrySet().iterator();

		byte destinationType = messageSend.getDestination().getDestinationType();

		if (destinationType == ActiveMQDestination.TOPIC_TYPE) {
			
			log.info("topic into queue");
			String physicalName = messageSend.getDestination().getPhysicalName();
			while (iterator.hasNext()) {

				Entry<String, List<String>> entry = iterator.next();

				if (physicalName.startsWith(".")) {
					physicalName = physicalName.substring(1, physicalName.length());
				}

				log.info(physicalName + " compare " + entry.getKey());

				if (MatchUtil.isMQTTTopicMatch(StringUtils.split(physicalName, "."),
						StringUtils.split(entry.getKey(), "."))) {

					log.info("match");
					List<String> queues = entry.getValue();
					
					log.info(queues);

					Iterator<String> queuesIterator = queues.iterator();

					while (queuesIterator.hasNext()) {
						Message messageCopy = messageSend.copy();
						String queueDest = queuesIterator.next();
						messageCopy.setDestination(
								ActiveMQDestination.createDestination(queueDest, ActiveMQDestination.QUEUE_TYPE));

						log.info("from " + messageSend.getDestination().getQualifiedName() + " into " + queueDest);

						super.send(producerExchange, messageCopy);
					}
				}
			}
			return;
		}

		super.send(producerExchange, messageSend);
	}
}
