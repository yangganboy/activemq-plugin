package org.yg.activemq.filter;

import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerFilter;
import org.apache.activemq.broker.ConnectionContext;
import org.apache.activemq.broker.ProducerBrokerExchange;
import org.apache.activemq.broker.region.Subscription;
import org.apache.activemq.command.ConsumerInfo;
import org.apache.activemq.command.Message;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.yg.activemq.pojo.RW;
import org.yg.activemq.service.IMQTTVerifyService;

import lombok.Setter;

public class DestAuthFilter extends BrokerFilter {

	@Setter
	private IMQTTVerifyService imqttVerifyService;

	private Logger log = LogManager.getLogger(this.getClass());

	public DestAuthFilter(Broker next) {
		super(next);
	}

	@Override
	public void send(ProducerBrokerExchange producerExchange, Message messageSend) throws Exception {

		log.info("Send " + messageSend.getDestination().getPhysicalName() + " "
				+ messageSend.getDestination().getQualifiedName());

		if (imqttVerifyService.jwtDestVerify(producerExchange.getConnectionContext(),
				messageSend.getDestination().getPhysicalName(), messageSend.getDestination().getDestinationType(),
				RW.W)) {
			log.info("send success");
			super.send(producerExchange, messageSend);
			return;
		}

		log.info("client" + producerExchange.getConnectionContext().getClientId() + " have not auth "
				+ messageSend.getDestination().getPhysicalName());
	}

	@Override
	public Subscription addConsumer(ConnectionContext context, ConsumerInfo info) throws Exception {
		log.info("Subscribe " + info.getDestination().getPhysicalName() + " "
				+ info.getDestination().getQualifiedName());
		if (imqttVerifyService.jwtDestVerify(context, info.getDestination().getPhysicalName(),
				info.getDestination().getDestinationType(), RW.R)) {
			log.info("Subscribe " + info.getDestination().getPhysicalName() + " success");
			return super.addConsumer(context, info);
		} else {
			return null;
		}
	}
}
