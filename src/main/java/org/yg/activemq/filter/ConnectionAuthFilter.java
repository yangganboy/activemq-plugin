package org.yg.activemq.filter;

import java.util.Collection;
import java.util.Objects;

import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerFilter;
import org.apache.activemq.broker.ConnectionContext;
import org.apache.activemq.command.ConnectionInfo;
import org.apache.activemq.state.ConnectionState;
import org.apache.activemq.state.SessionState;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.yg.activemq.service.IMQTTVerifyService;

import lombok.NonNull;
import lombok.Setter;

public class ConnectionAuthFilter extends BrokerFilter {

	private IMQTTVerifyService imqttVerifyService;

	private Logger log = LogManager.getLogger(this.getClass());

	@Setter
	private String username;

	@Setter
	private String password;

	public ConnectionAuthFilter(@NonNull Broker next, @NonNull IMQTTVerifyService imqttVerifyService) {
		super(next);

		this.imqttVerifyService = imqttVerifyService;
	}

	@Override
	public void addConnection(ConnectionContext context, ConnectionInfo info) throws Exception {

		String clientUsername = info.getUserName();

		Objects.requireNonNull(clientUsername);

		boolean flag = false;

		if (username.equals(clientUsername) && password.equals(info.getPassword())) {
			flag = true;
		} else {
			if (imqttVerifyService.jwtLoginVerify(info.getUserName(), info)) {
				flag = true;
			}

		}

		if (flag == false) {
			log.debug("client " + context.getClientId() + " login fail");
			return;
		}

		super.addConnection(context, info);
	}

	@Override
	public void removeConnection(ConnectionContext context, ConnectionInfo info, Throwable error) throws Exception {
		imqttVerifyService.clearVerify(info);
		super.removeConnection(context, info, error);
	}
}
