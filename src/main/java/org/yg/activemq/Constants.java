package org.yg.activemq;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class Constants {
	public final  static Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;
}
