package org.yg.activemq.plugin;

import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerPlugin;
import org.springframework.context.ApplicationContext;
import org.yg.activemq.SpringApplicationHolder;
import org.yg.activemq.filter.ConnectionAuthFilter;
import org.yg.activemq.service.IMQTTVerifyService;

import lombok.Setter;

public class ConnectionAuthPlugin implements BrokerPlugin {

	@Setter
	private String username;

	@Setter
	private String password;

	@Override
	public Broker installPlugin(Broker broker) throws Exception {
		ApplicationContext applicationContext = SpringApplicationHolder.getApplicationContext();
		ConnectionAuthFilter connectionAuthFilter = new ConnectionAuthFilter(broker,
				applicationContext.getBean(IMQTTVerifyService.class));
		connectionAuthFilter.setUsername(username);
		connectionAuthFilter.setPassword(password);
		return connectionAuthFilter;
	}
	
}
