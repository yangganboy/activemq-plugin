package org.yg.activemq.plugin;

import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerPlugin;
import org.yg.activemq.SpringApplicationHolder;
import org.yg.activemq.filter.DestAuthFilter;
import org.yg.activemq.service.MQTTVerifyService;

public class DestAuthPlugin implements BrokerPlugin {

	@Override
	public Broker installPlugin(Broker broker) throws Exception {
		DestAuthFilter mqttTopicAuthFilter = new DestAuthFilter(broker);
		mqttTopicAuthFilter.setImqttVerifyService(
				SpringApplicationHolder.getApplicationContext().getBean(MQTTVerifyService.class));
		return mqttTopicAuthFilter;
	}
}
