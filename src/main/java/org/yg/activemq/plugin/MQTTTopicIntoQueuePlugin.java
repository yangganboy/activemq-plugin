package org.yg.activemq.plugin;

import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerPlugin;
import org.yg.activemq.filter.MQTTTopicIntoQueueFilter;

import lombok.Setter;

public class MQTTTopicIntoQueuePlugin implements BrokerPlugin {

	@Setter
	private String transpondRuleStr;

	@Override
	public Broker installPlugin(Broker broker) throws Exception {
		MQTTTopicIntoQueueFilter mqttTopicIntoQueueFilter = new MQTTTopicIntoQueueFilter(broker);
		mqttTopicIntoQueueFilter.setTranspondRule(transpondRuleStr);
		return mqttTopicIntoQueueFilter;
	}

}
