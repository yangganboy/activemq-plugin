package org.yg.activemq.util;

import lombok.NonNull;

public class MatchUtil {
	
	public static boolean isMQTTTopicMatch(@NonNull String[] topic, @NonNull String[] filter) {


		if (topic.length != filter.length) {
			if (filter[filter.length - 1].equals("#") == false) {
				return false;
			}
		}

		for (int i = 0; i < filter.length; i++) {
			String string0 = topic[i];
			String string1 = filter[i];

			if (string0.length() != string1.length()) {
				return false;
			}

			if (string0.equals(string1)) {
				continue;
			} else {
				for (int j = 0; j < string0.length(); j++) {
					if ((string1.charAt(j) == '$') == false && string0.charAt(j) != string1.charAt(j)) {
						return false;
					}
				}
			}
		}

		return true;
	}
	
	public static boolean isActivemqPhysicsDestinationMatch(@NonNull String[] destination, @NonNull String[] filter) {

		if (destination.length == filter.length) {
			for (int i = 0; i < destination.length; i++) {
				if (filter[i].equals("*") == false && filter[i].equals(destination[i]) == false) {
					return false;
				}
			}
			return true;
		}
		
		return false;
	}
}
