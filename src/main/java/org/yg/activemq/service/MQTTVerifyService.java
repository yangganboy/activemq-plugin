package org.yg.activemq.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.activemq.broker.ConnectionContext;
import org.apache.activemq.command.ActiveMQDestination;
import org.apache.activemq.command.ConnectionInfo;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.yg.activemq.pojo.RW;
import org.yg.activemq.util.MatchUtil;

import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@NoArgsConstructor
public class MQTTVerifyService implements IMQTTVerifyService {

	private Logger log = LogManager.getLogger(this.getClass());

	@Setter
	private String salt0;

	@Setter
	private byte maxErrorCount;

	@Setter
	private long errorWaitTime;

	@Setter
	private String jwtSecretKey;

	@Setter
	private String jwtSecretKey2;

	private ConcurrentHashMap<String, Map<String, List<String>>> authInfo = new ConcurrentHashMap<>();

	@Override
	public boolean jwtLoginVerify(@NonNull String jwt, ConnectionInfo info) {

		try {
			Jwts.parser().setSigningKey(jwtSecretKey).parse(info.getUserName());
			return true;
		} catch (JwtException e) {
			log.debug("", e);
			try {
				Jwts.parser().setSigningKey(jwtSecretKey2).parse(info.getUserName());
				return true;
			} catch (Exception e2) {
				log.debug("", e2);
			}
		}
		return false;
	}

	private Map<String, List<String>> setAuthInfo(ConnectionInfo info) {
		Jwt parse = null;
		try {
			parse = Jwts.parser().setSigningKey(jwtSecretKey).parse(info.getUserName());
		} catch (SignatureException e) {
			parse = Jwts.parser().setSigningKey(jwtSecretKey2).parse(info.getUserName());
		}

		Header header = parse.getHeader();

		Map<String, List<String>> map = new HashMap<>();
		String allowTopic = (String) header.get("allowTopic");
		if (allowTopic != null) {
			String[] allowTopicArray = StringUtils.split(allowTopic, ",");
			map.put("allowTopic", new ArrayList<>(Arrays.asList(allowTopicArray)));
		}

		String allowQueue = (String) header.get("allowQueue");

		if (allowQueue != null) {
			String[] allowQueueArray = StringUtils.split(allowQueue, ",");
			map.put("allowQueue", Arrays.asList(allowQueueArray));
		}

		authInfo.put(info.getConnectionId().getValue(), map);

		return map;
	}

	@Override
	public boolean jwtDestVerify(ConnectionContext info, String dest, byte destType, RW rw) {

		log.info("destType " + destType);

		Map<String, List<String>> map = authInfo.get(info.getConnectionId().getValue());

		if (map == null) {
			try {
				map = setAuthInfo(info.getConnectionState().getInfo());
			} catch (JwtException e) {
				log.debug(e);
				return false;
			}
		}

		log.info("authInfo: " + map.toString());

		switch (destType) {
		case ActiveMQDestination.QUEUE_TYPE: {
			List<String> allowQueues = map.get("allowQueue");
			if (allowQueues != null) {
				String[] dests = StringUtils.split(dest, ".");
				for (int i = 0; i < allowQueues.size(); i++) {
					String rwMatch = isRWMatch(allowQueues.get(i), rw);
					if (rwMatch != null) {
						if (MatchUtil.isActivemqPhysicsDestinationMatch(dests, StringUtils.split(rwMatch, "."))) {
							return true;
						}
					}
				}
			}
			break;
		}
		case ActiveMQDestination.TOPIC_TYPE: {
			List<String> allowDests = map.get("allowTopic");
			if (allowDests != null) {
				String[] dests = StringUtils.split(dest, ".");
				for (int i = 0; i < allowDests.size(); i++) {
					String rwMatch = isRWMatch(allowDests.get(i), rw);
					String[] matchs = StringUtils.split(rwMatch, ".");
					if (rwMatch != null) {
						if (MatchUtil.isActivemqPhysicsDestinationMatch(dests, matchs)) {
							return true;
						}
						if (MatchUtil.isMQTTTopicMatch(dests, matchs)) {
							return true;
						}
					}
				}
			}
			break;
		}
		default:
			log.info("type" + destType + " is not match");
			break;
		}
		return false;

	}

	private String isRWMatch(String filter, RW rw) {
		log.info("filter " + filter);

		if (filter.endsWith("/W")) {
			if (rw == RW.W) {
				return filter.substring(0, filter.length() - 2);
			} else {
				return null;
			}

		} else if (filter.endsWith("/R")) {
			if (rw == RW.R) {
				return filter.substring(0, filter.length() - 2);
			} else {
				return null;
			}
		}
		return filter;
	}

	@Override
	public void clearVerify(@NonNull ConnectionInfo info) {
		authInfo.remove(info.getConnectionId().getValue());
	}

}
