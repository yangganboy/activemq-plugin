package org.yg.activemq.service;

import org.apache.activemq.broker.ConnectionContext;
import org.apache.activemq.command.ConnectionInfo;
import org.yg.activemq.pojo.RW;

import lombok.NonNull;

public interface IMQTTVerifyService {

	boolean jwtLoginVerify(@NonNull String jwt, @NonNull ConnectionInfo info);

	boolean jwtDestVerify(@NonNull ConnectionContext info, @NonNull String dest, byte destType, @NonNull RW rw);

	void clearVerify(@NonNull ConnectionInfo info);

}
