package org.yg.activemq.exception;

public class OptimisticLockException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6925671593902601749L;

	public OptimisticLockException() {
		super();
	}

	public OptimisticLockException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public OptimisticLockException(String message, Throwable cause) {
		super(message, cause);
	}

	public OptimisticLockException(String message) {
		super(message);
	}

	public OptimisticLockException(Throwable cause) {
		super(cause);
	}

}
